<?php

/**
 * @file
 * Integrates Unzer's redirected payment service with Ubercart.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Original author: Roland Gruber
 */

/**
 * Menu entries for Unzer module.
 *
 * Implements hook_menu().
 *
 * @return array
 *   menu entries
 */
function uc_heidelpay_menu() {
  $items['cart/heidelpay/complete'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_heidelpay_complete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/store/orders/%uc_order/heidelpay'] = array(
    'title' => 'Unzer',
    'page callback' => 'uc_heidelpay_get_log',
    'page arguments' => array(3),
    'access arguments' => array('view all orders'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 7,
  );
  $items['admin/store/settings/heidelpay'] = array(
    'title' => 'Unzer configuration',
    'description' => 'Manages Unzer merchant accounts',
    'page callback' => 'uc_heidelpay_configure_methods',
    'access arguments' => array('configure heidelpay settings'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'uc_heidelpay.admin.inc',
  );
  $items['admin/store/settings/heidelpay/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file' => 'uc_heidelpay.admin.inc',
    'weight' => -10,
  );
  $items['admin/store/settings/heidelpay/add'] = array(
    'title' => 'Add new payment method',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_heidelpay_add_payment_method_form'),
    'access arguments' => array('configure heidelpay settings'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'uc_heidelpay.admin.inc',
    'weight' => 5,
  );
  $items['admin/store/settings/heidelpay/%uc_heidelpay_payment_method/edit'] = array(
    'title' => 'Edit payment method',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_heidelpay_edit_payment_method_form', 4),
    'access arguments' => array('configure heidelpay settings'),
    'type' => MENU_CALLBACK,
    'file' => 'uc_heidelpay.admin.inc',
  );
  $items['admin/store/settings/heidelpay/%uc_heidelpay_payment_method/delete'] = array(
    'title' => 'Delete payment method',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_heidelpay_delete_payment_method_form', 4),
    'access arguments' => array('configure heidelpay settings'),
    'type' => MENU_CALLBACK,
    'file' => 'uc_heidelpay.admin.inc',
  );
  return $items;
}

/**
 * Menu load handler for payment method.
 */
function uc_heidelpay_payment_method_load($arg) {
  $methods = variable_get('uc_heidelpay_configured_payment_methods', array('default' => t('Default')));
  if (in_array($arg, array_keys($methods))) {
    return $arg;
  }
  return FALSE;
}

/**
 * Implements hook_permissions().
 */
function uc_heidelpay_permissions() {
  return array(
    'configure heidelpay settings' => array(
      'title' => t('Configure Unzer settings'),
      'description' => t('Set up additional Unzer payment methods.'),
    ),
  );
}

/**
 * Implements hook_uc_payment_method().
 */
function uc_heidelpay_uc_payment_method() {
  $methods = array();
  $payment_methods = variable_get('uc_heidelpay_configured_payment_methods', array('default' => t('Default')));
  foreach ($payment_methods as $id => $method) {
    $methods[] = array(
      'id' => 'heidelpay_' . $id,
      'name' => t('Unzer - !method', array('!method' => $method)),
      'title' => theme('uc_heidelpay_cards', array('heidelpay_' . $id)),
      'desc' => t('Redirect to Unzer to pay by credit card.'),
      'callback' => 'uc_heidelpay_payment_method',
      'weight' => 3,
      'checkout' => TRUE,
      'no_gateway' => TRUE,
    );
  }
  return $methods;
}

/**
 * Returns HTML for the payment options.
 *
 * @param array $args
 *   Theme arguments.
 *
 * @ingroup themeable
 */
function theme_uc_heidelpay_cards($args) {
  $id = $args[0];
  $path = drupal_get_path('module', 'uc_heidelpay');
  drupal_add_css($path . '/css/uc_heidelpay.css');
  $title = variable_get('uc_heidelpay_method_title', t('Payments powered by Unzer'));

  $enabled_payment_methods = variable_get(
    'uc_heidelpay_' . $id . '_payment_methods',
    array('visa', 'mastercard')
  );
  $label = t('Credit card');
  if (in_array('sofort', $enabled_payment_methods)) {
    $label = t('Sofort');
  }
  elseif (in_array('ideal', $enabled_payment_methods)) {
    $label = t('iDEAL');
  }
  $output = $label . ' ';
  foreach ($enabled_payment_methods as $method) {
    $output .= theme('image',
      array(
        'path' => $path . '/images/' . $method . '.png',
        'alt' => $method,
        'attributes' => array('class' => 'uc-heidelpay-card-type'),
      )
    );
  }
  $output .= '&nbsp;&nbsp;' . theme('image',
    array(
      'path' => $path . '/images/unzer.png',
      'alt' => $title,
      'width' => '61',
      'height' => '28',
      'attributes' => array('class' => 'uc-heidelpay-logo'),
    )
  );

  return $output;
}

/**
 * Payment method settings.
 */
function uc_heidelpay_payment_method($op, &$order, $form = NULL, &$form_state = NULL) {
  switch ($op) {
    case 'cart-process':
      $_SESSION['pay_method'] = $form_state['values']['panes']['payment']['payment_method'];
      break;

    case 'settings':
      if (isset($form_state['build_info']['args'][0])) {
        $method = $form_state['build_info']['args'][0];
        $form['#attached']['js'][] = array(
          'data' => drupal_get_path('module', 'uc_heidelpay') . '/uc_heidelpay.js',
          'type' => 'file',
        );
        // Setting array_filter filters the array removing items where the value
        // is either '', NULL or FALSE.
        $form['array_filter'] = array(
          '#type' => 'value',
          '#value' => TRUE,
        );
        $form['help_text']['heidelpay_settings'] = array(
          '#type' => 'item',
          '#prefix' => '<div class="help">',
          '#markup' => t('<p>For this module to work properly you must configure a few options with data provided by Unzer:</p>'),
          '#suffix' => '</div>',
        );
        $form['uc_heidelpay_' . $method . '_sid'] = array(
          '#type' => 'textfield',
          '#title' => t('Sender ID'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_sid', ''),
          '#required' => TRUE,
        );
        $form['uc_heidelpay_' . $method . '_user'] = array(
          '#type' => 'textfield',
          '#title' => t('User ID'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_user', ''),
          '#required' => TRUE,
        );
        $form['uc_heidelpay_' . $method . '_password'] = array(
          '#type' => 'textfield',
          '#title' => t('User password'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_password', ''),
          '#required' => TRUE,
        );
        $form['uc_heidelpay_' . $method . '_channel'] = array(
          '#type' => 'textfield',
          '#title' => t('Channel ID'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_channel', ''),
          '#required' => TRUE,
        );
        $form['uc_heidelpay_' . $method . '_checkout_button'] = array(
          '#type' => 'textfield',
          '#title' => t('Order review submit button text'),
          '#description' => t('Alter the text of the submit button on the review order page.'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_checkout_button', t('Submit Order')),
        );
        $form['uc_heidelpay_' . $method . '_hso_message'] = array(
          '#type' => 'textfield',
          '#title' => t('Payment message'),
          '#description' => t('Message to display on Unzer payment page.'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_hso_message', t('Thank you very much for your order. Please enter your credit card data below.')),
        );
        $form['uc_heidelpay_' . $method . '_css'] = array(
          '#type' => 'textfield',
          '#title' => t('CSS URL'),
          '#description' => t('CSS file for Unzer payment page.'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_css', ''),
        );
        $form['uc_heidelpay_' . $method . '_debug'] = array(
          '#type' => 'select',
          '#title' => t('Debug mode'),
          '#multiple' => FALSE,
          '#options' => array(
            'log' => t('Log'),
            'none' => t('None'),
          ),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_debug', 'log'),
        );
        $form['payment_methods'] = array(
          '#type' => 'fieldset',
          '#title' => t('Payment methods'),
          '#description' => t('Select the payment methods to display in checkout.'),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );
        $form['payment_methods']['uc_heidelpay_' . $method . '_payment_methods'] = array(
          '#type' => 'checkboxes',
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_payment_methods',
              array(
                'visa',
                'visa-electron',
                'mastercard',
                'masterpass',
                'maestro',
              )
          ),
          '#options' => _uc_heidelpay_payment_card_types(),
        );
        $form['payment_parameters'] = array(
          '#type' => 'fieldset',
          '#title' => t('Payment parameters'),
          '#description' => t('These options control what parameters are sent to Unzer when the customer submits the order.'),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );
        $form['payment_parameters']['uc_heidelpay_' . $method . '_test'] = array(
          '#type' => 'checkbox',
          '#title' => t('Enable test mode'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_test', TRUE),
        );
        $form['payment_parameters']['uc_heidelpay_' . $method . '_cancel_order'] = array(
          '#type' => 'checkbox',
          '#title' => t('Cancel order in Ubercart if cancelled during payment'),
          '#description' => t("If the customer cancels out of payment processing whilst on the Unzer server, remove the items from their cart and cancel their order in Ubercart."),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_cancel_order', FALSE),
        );
        $form['payment_parameters']['uc_heidelpay_' . $method . '_lang'] = array(
          '#type' => 'textfield',
          '#title' => t('Payment page language'),
          '#description' => t('Specify the payment page language. Enter a 2-character language code. For example "en" specifies English.'),
          '#size' => 8,
          '#maxlength' => 2,
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_lang', 'en'),
        );
        $form['payment_urls'] = array(
          '#type' => 'fieldset',
          '#title' => t('Payment URLs'),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );
        $form['payment_urls']['uc_heidelpay_' . $method . '_callbackURL'] = array(
          '#type' => 'textfield',
          '#title' => t('Callback URL'),
          '#description' => t('Callback URL for redirect from Unzer to shop, e.g. "https://www.example.com/cart/heidelpay/complete".'),
          '#maxlength' => 1024,
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_callbackURL', url('/cart/heidelpay/complete', array('absolute' => TRUE))),
          '#required' => TRUE,
        );
        $form['payment_urls']['uc_heidelpay_' . $method . '_test_url'] = array(
          '#type' => 'textfield',
          '#title' => t('Test URL'),
          '#description' => t('The Unzer test environment URL.'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_test_url', 'https://test-heidelpay.hpcgw.net/sgw/gtwu'),
          '#element_validate' => array('uc_heidelpay_valid_url'),
          '#required' => TRUE,
        );
        $form['payment_urls']['uc_heidelpay_' . $method . '_live_url'] = array(
          '#type' => 'textfield',
          '#title' => t('Live URL'),
          '#description' => t('The Unzer live environment URL.'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_live_url', 'https://heidelpay.hpcgw.net/sgw/gtwu'),
          '#element_validate' => array('uc_heidelpay_valid_url'),
          '#required' => TRUE,
        );
        $form['payment_urls']['uc_heidelpay_' . $method . '_signature_password'] = array(
          '#type' => 'textfield',
          '#title' => t('Signature password'),
          '#description' => t('Password to sign orders. Used to verify response from payment system.'),
          '#default_value' => variable_get('uc_heidelpay_' . $method . '_signature_password', md5(drupal_random_bytes(32))),
          '#required' => TRUE,
        );
      }
      return $form;
  }
}

/**
 * Implements hook_form_alter().
 */
function uc_heidelpay_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);
    if (strpos($order->payment_method, 'heidelpay') !== FALSE) {
      $form['actions']['submit']['#access'] = FALSE;
      $form['#prefix'] = '<table id="uc-heidelpay-review-table"><tr><td>';
      $uc_heidelpay_form = drupal_get_form('uc_heidelpay_form', $order);
      $form['#suffix'] = '</td><td>' . drupal_render($uc_heidelpay_form) . '</td></tr></table>';
    }
  }
}

/**
 * Processes the payment and completes the order.
 */
function uc_heidelpay_complete() {
  $order_id = $_POST['IDENTIFICATION_TRANSACTIONID'];
  $order = uc_order_load($order_id);
  if ($order == NULL) {
    watchdog('uc_heidelpay', 'Unable to load order %s',
      array('%s' => $order_id));
    echo url('/cart', array('absolute' => TRUE));
    return;
  }
  $uc_cart_id = $_POST['CRITERION_CART_ID'];
  $debugConfig = variable_get('uc_heidelpay_' . $order->payment_method . '_debug', 'log');
  $debug = ($debugConfig == 'log') ? TRUE : FALSE;
  if ($debug) {
    watchdog('uc_heidelpay', 'POST parameters: %s',
      array('%s' => str_replace("\n", '', print_r($_POST, TRUE))));
  }

  if (!uc_heidelpay_is_valid_payment_response($order->payment_method, $debug, $order_id)) {
    echo url('/cart', array('absolute' => TRUE));
    return;
  }
  $paymentAccepted = uc_heidelpay_is_payment_accepted($debug);
  uc_heidelpay_store_transaction_record($order_id);
  if ($paymentAccepted) {
    uc_heidelpay_process_payment_ok($debug, $order_id, $uc_cart_id);
  }
  else {
    uc_heidelpay_process_cancel($debug, $order_id, $uc_cart_id);
  }
}

/**
 * Checks if the payment POST request from Unzer was valid.
 *
 * @param string $method
 *   Payment method.
 * @param bool $debug
 *   Debug mode enabled.
 * @param string $order_id
 *   Order ID.
 *
 * @return bool
 *   Valid response.
 */
function uc_heidelpay_is_valid_payment_response($method, $debug, $order_id) {
  // Check signature.
  $amount = $_POST['PRESENTATION_AMOUNT'];
  $currency = $_POST['PRESENTATION_CURRENCY'];
  $signature = uc_heidelpay_calculate_signature(variable_get('uc_heidelpay_' . $method . '_signature_password', ''), $order_id, $amount, $currency);
  if ($_POST['CRITERION_SIGNATURE'] != $signature) {
    watchdog('uc_heidelpay', 'Invalid signature.');
    return FALSE;
  }
  if ($debug) {
    watchdog('uc_heidelpay', 'Signature matched for order %s',
      array('%s' => $order_id));
  }
  return TRUE;
}

/**
 * Returns a signature to verify the response from payment system.
 *
 * @param string $password
 *   Payment signature password.
 * @param string $order_id
 *   Order ID.
 * @param string $amount
 *   Amount from order.
 * @param string $currency
 *   Currency from order.
 */
function uc_heidelpay_calculate_signature($password, $order_id, $amount, $currency) {
  $signatureParameters = array(
    $order_id,
    $amount,
    strtoupper($currency),
    $password,
  );
  $signaturePlainText = implode('#', $signatureParameters);
  return drupal_hash_base64($signaturePlainText);
}

/**
 * Returns if the payment was processed by Unzer.
 *
 * @param bool $debug
 *   Debug mode enabled.
 *
 * @return bool
 *   Accepted and processed.
 */
function uc_heidelpay_is_payment_accepted($debug) {
  $trans_status = $_POST['PROCESSING_RESULT'];
  $accepted = ($trans_status == 'ACK');
  if ($debug) {
    $text = $accepted ? 'accepted' : 'canceled';
    watchdog('uc_heidelpay', 'Payment %s',
      array('%s' => $text));
  }
  return $accepted;
}

/**
 * Stores the database record that includes all transaction data.
 *
 * @param string $order_id
 *   Order ID.
 */
function uc_heidelpay_store_transaction_record($order_id) {
  $time = new DateTime('now', new DateTimeZone('UTC'));
  $payment_methods = variable_get('uc_heidelpay_configured_payment_methods', array('default' => t('Default')));
  $methodID = substr($_POST['CRITERION_METHOD'], strlen('heidelpay_'));
  $paymentRecord = array(
    'order_id'                  => $order_id,
    'transtime'                 => $time->format('y-m-d H:i'),
    'profile'                   => $payment_methods[$methodID],
  );
  $parameters = array(
    'identification_id'         => 'IDENTIFICATION_UNIQUEID',
    'identification_id_short'   => 'IDENTIFICATION_SHORTID',
    'cardtype'                  => 'ACCOUNT_BRAND',
    'processing_result'         => 'PROCESSING_RESULT',
    'authamount'                => 'PRESENTATION_AMOUNT',
    'authcurrency'              => 'PRESENTATION_CURRENCY',
    'processing_status_message' => 'PROCESSING_STATUS',
    'processing_status_code'    => 'PROCESSING_STATUS_CODE',
    'processing_reason_message' => 'PROCESSING_REASON',
    'processing_reason_code'    => 'PROCESSING_REASON_CODE',
    'processing_return_message' => 'PROCESSING_RETURN',
    'processing_return_code'    => 'PROCESSING_RETURN_CODE',
    'risk_score'                => 'PROCESSING_RISK_SCORE',
    'validation_code'           => 'VALIDATOR-RM_AC_REASON_CODE',
    'validation_reason'         => 'VALIDATOR_RM_AC_REASON',
    'blacklist_code'            => 'VALIDATOR_RM_BL_REASON_CODE',
    'blacklist_reason'          => 'VALIDATOR_RM_BL_REASON',
  );
  foreach ($parameters as $dbKey => $postKey) {
    if (!empty($_POST[$postKey])) {
      $paymentRecord[$dbKey] = $_POST[$postKey];
    }
  }
  drupal_write_record('uc_payment_heidelpay', $paymentRecord);
}

/**
 * Processes the canceling of the payment by Unzer.
 *
 * @param bool $debug
 *   Debug mode enabled.
 * @param string $order_id
 *   Order ID.
 * @param string $uc_cart_id
 *   Ubercart cart ID.
 */
function uc_heidelpay_process_cancel($debug, $order_id, $uc_cart_id) {
  if ($debug) {
    watchdog('uc_heidelpay', 'Payment canceled');
  }
  $methodID = $_POST['CRITERION_METHOD'];
  uc_order_comment_save($order_id, 0, t('Payment cancelled by user.'), 'admin');
  if (variable_get('uc_heidelpay_' . $methodID . '_cancel_order', FALSE)) {
    uc_order_update_status($order_id, 'canceled');
    uc_cart_empty($uc_cart_id);
    watchdog('uc_heidelpay', 'Cart emptied');
  }
  echo url('/cart', array('absolute' => TRUE));
}

/**
 * Processes the successful payment by Unzer.
 *
 * @param bool $debug
 *   Debug mode enabled.
 * @param string $order_id
 *   Order ID.
 * @param string $uc_cart_id
 *   Ubercart cart ID.
 */
function uc_heidelpay_process_payment_ok($debug, $order_id, $uc_cart_id) {
  if ($debug) {
    watchdog('uc_heidelpay', 'Payment accepted');
  }
  $amount = $_POST['PRESENTATION_AMOUNT'];
  $comment = t('Paid by Unzer order #@order.', array('@order' => $order_id));
  uc_payment_enter($order_id, 'Unzer', $amount, 0, NULL, $comment);
  echo url('/cart/checkout/complete', array('absolute' => TRUE));
}

/**
 * Implements hook_theme().
 */
function uc_heidelpay_theme() {
  return array(
    'uc_heidelpay_cards' => array(
      'variables' => array('method' => NULL),
    ),
  );
}

/**
 * Prepares and returns the form for POSTing to Unzer.
 */
function uc_heidelpay_form($form, $form_state, $order) {
  try {
    $targetURL = uc_heidelpay_get_target_url($order);
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
    return array();
  }

  $form = array();

  $method = $order->payment_method;

  $form['#action'] = $targetURL;
  $form['submit'] = array(
    '#type' => 'submit',
    '#name' => '',
    '#value' => variable_get('uc_heidelpay_' . $method . '_checkout_button', t('Submit Order')),
  );

  $_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_complete'] = TRUE;
  $_SESSION['do_review'] = TRUE;
  return $form;
}

/**
 * Sends the initial POST request to Unzer to get the redirect URL.
 *
 * @param mixed $order
 *   Ubercart order.
 */
function uc_heidelpay_get_target_url($order) {
  $method = $order->payment_method;
  $cart_contents = uc_cart_get_contents();
  foreach ($cart_contents as $item) {
    $cart_items[] = $item->qty . 'x ' . $item->title;
  }
  $usage = implode(", ", $cart_items);
  if (strlen($usage) > 80) {
    $usage = substr($usage, 0, 77) . '...';
  }
  $country_data = uc_get_country_data(array('country_id' => $order->billing_country));
  $country = $country_data[0]['country_iso_code_2'];
  $zone = uc_get_zone_code($order->billing_zone);

  $enabled_payment_methods = variable_get(
      'uc_heidelpay_' . $method . '_payment_methods',
      array('visa', 'mastercard')
      );
  $paymentType = 'cc';
  if (in_array('sofort', $enabled_payment_methods)) {
    $paymentType = 'sofort';
  }
  elseif (in_array('ideal', $enabled_payment_methods)) {
    $paymentType = 'ideal';
  }

  $data = array(
    'REQUEST.VERSION' => '1.0',
    'TRANSACTION.CHANNEL' => variable_get('uc_heidelpay_' . $method . '_channel'),
    'IDENTIFICATION.TRANSACTIONID' => $order->order_id,
    'TRANSACTION.MODE' => 'LIVE',
    'PRESENTATION.AMOUNT' => uc_heidelpay_format_amount($order->order_total),
    'PRESENTATION.CURRENCY' => variable_get('uc_currency_code', 'USD'),
    'PRESENTATION.USAGE' => $usage,
    'SECURITY.SENDER' => variable_get('uc_heidelpay_' . $method . '_sid'),
    'USER.LOGIN' => variable_get('uc_heidelpay_' . $method . '_user'),
    'USER.PWD' => variable_get('uc_heidelpay_' . $method . '_password'),
    'FRONTEND.ENABLED' => 'true',
    'FRONTEND.POPUP' => 'false',
    'FRONTEND.MODE' => 'DEFAULT',
    'FRONTEND.REDIRECT_TIME' => '3',
    'FRONTEND.RESPONSE_URL' => variable_get('uc_heidelpay_' . $method . '_callbackURL'),
    'FRONTEND.LANGUAGE' => variable_get('uc_heidelpay_' . $method . '_lang'),
    'ACCOUNT.HOLDER' => $order->billing_first_name . ' ' . $order->billing_last_name,
    'NAME.GIVEN' => $order->billing_first_name,
    'NAME.FAMILY' => $order->billing_last_name,
    'NAME.COMPANY' => $order->billing_company,
    'ADDRESS.STREET' => $order->billing_street1,
    'ADDRESS.CITY' => $order->billing_city,
    'ADDRESS.ZIP' => $order->billing_postal_code,
    'CONTACT.EMAIL' => $order->primary_email,
    'CONTACT.PHONE' => $order->billing_phone,
    'ADDRESS.COUNTRY' => $country,
    'ADDRESS.STATE' => $zone,
    'CRITERION.CART_ID' => uc_cart_get_id(),
    'CRITERION.METHOD' => $method,
    'CRITERION.SIGNATURE' => uc_heidelpay_calculate_signature(variable_get('uc_heidelpay_' . $method . '_signature_password', ''), $order->order_id, uc_heidelpay_format_amount($order->order_total), variable_get('uc_currency_code', 'USD')),
  );
  // payment code
  switch ($paymentType) {
    case 'cc':
      $data['PAYMENT.CODE'] = 'CC.DB';
      $data['FRONTEND.PM.1.METHOD'] = 'CC';
      $data['FRONTEND.PM.1.ENABLED'] = 'true';
      $data['ACCOUNT.BRAND'] = 'VISA';
      break;
    case 'sofort':
      $data['PAYMENT.CODE'] = 'OT.PA';
      $data['ACCOUNT.BRAND'] = 'SOFORT';
      break;
    case 'ideal':
      $data['PAYMENT.CODE'] = 'OT.PA';
      $data['ACCOUNT.BRAND'] = 'IDEAL';
      break;
  }
  // Payment message.
  $paymentMessage = variable_get('uc_heidelpay_' . $method . '_hso_message', '');
  if (!empty($paymentMessage)) {
    $data['FRONTEND.MESSAGE.POSITION'] = 'TOP';
    $data['FRONTEND.MESSAGE.TYPE'] = 'INFO';
    $data['FRONTEND.MESSAGE.TEXT'] = $paymentMessage;
  }
  // CSS URL.
  $cssUrl = variable_get('uc_heidelpay_' . $method . '_css', '');
  if (!empty($cssUrl)) {
    $data['FRONTEND.CSS_PATH'] = $cssUrl;
  }
  $query = drupal_http_build_query($data);
  $test_server = variable_get('uc_heidelpay_' . $method . '_test_url', 'https://test-heidelpay.hpcgw.net/sgw/gtwu');
  $live_server = variable_get('uc_heidelpay_' . $method . '_live_url', 'https://heidelpay.hpcgw.net/sgw/gtwu');
  $url = (variable_get('uc_heidelpay_' . $method . '_test', TRUE)) ? $test_server : $live_server;
  $requestOptions = array(
    'method' => 'POST',
    'data' => $query,
    'timeout' => 10,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );
  $result = drupal_http_request($url, $requestOptions);
  if (empty($result->data)) {
    watchdog('uc_heidelpay', 'Unable to get target URL, data is empty, code %s',
      array('%s' => $result->code));
    throw new UnexpectedValueException(t('Connection to payment provider failed. Please try again later.'), $result->code);
  }
  $resultData = drupal_get_query_array($result->data);
  $postValidation = $resultData['POST.VALIDATION'];
  if (($postValidation != 'ACK') || empty($resultData['FRONTEND.REDIRECT_URL'])) {
    watchdog('uc_heidelpay', 'Unable to get target URL. Post validation: %val Reason: %reason',
      array(
        '%val' => $postValidation,
        '%reason' => $resultData['PROCESSING.RETURN'],
      )
    );
    throw new UnexpectedValueException(t('Connection to payment provider failed. Please try again later.'), $result->code);
  }
  return $resultData['FRONTEND.REDIRECT_URL'];
}

/**
 * Formats the amount for sending to payment system.
 *
 * @param string $amount
 *   Amount of order.
 */
function uc_heidelpay_format_amount($amount) {
  return uc_currency_format($amount, FALSE, FALSE, '.');
}

/**
 * Returns the supported payment card types.
 *
 * @return array
 *   An array of supported card types.
 */
function _uc_heidelpay_payment_card_types() {
  return array(
    'visa' => t('Visa Credit and Debit'),
    'mastercard' => t('Mastercard'),
    'amex' => t('American Express'),
    'jcb' => t('JCB'),
    'diners' => t('Diners'),
    'ideal' => t('iDEAL'),
    'sofort' => t('Sofort'),
  );
}

/**
 * Validates a supplied URL using valid_url().
 */
function uc_heidelpay_valid_url($element, &$form_state) {
  if ($form_state['values']['op'] == t('Save configuration')) {
    if (isset($form_state['values'][$element['#name']])) {
      if (!valid_url($form_state['values'][$element['#name']], TRUE)) {
        form_set_error($element['#name'], t('The format of the @title appears to be invalid.', array('@title' => $element['#title'])));
      }
    }
  }
}

/**
 * Implements hook_views_api().
 */
function uc_heidelpay_views_api() {
  return array(
    'api' => '3.0',
    'path' => drupal_get_path('module', 'uc_heidelpay') . '/views',
  );
}

/**
 * Displays the Unzer order details like transaction ID.
 *
 * Callback function for menu admin/store/orders/%uc_order/heidelpay.
 *
 * @return array
 *   HTML code with order details.
 */
function uc_heidelpay_get_log($order) {
  $result = db_query("SELECT * FROM {uc_payment_heidelpay} WHERE order_id = :id", array(':id' => $order->order_id));
  $rows = array();
  $first = TRUE;
  $parameters = array(
    t('Identification ID')         => 'identification_id',
    t('Identification short ID')   => 'identification_id_short',
    t('Card type')                 => 'cardtype',
    t('Timestamp')                 => 'transtime',
    t('Amount')                    => 'authamount',
    t('Currency')                  => 'authcurrency',
    t('Processing result')         => 'processing_result',
    t('Processing status message') => 'processing_status_message',
    t('Processing status code')    => 'processing_status_code',
    t('Processing reson message')  => 'processing_reason_message',
    t('Processing reason code')    => 'processing_reason_code',
    t('Processing return message') => 'processing_return_message',
    t('Processing return code')    => 'processing_return_code',
    t('Risk score')                => 'risk_score',
    t('Validation reason code')    => 'validation_code',
    t('Validation reason')         => 'validation_reason',
    t('Blacklist code')            => 'blacklist_code',
    t('Blacklist reason')          => 'blacklist_reason',
    t('Payment profile')           => 'profile',
  );
  foreach ($result as $rowData) {
    if (!$first) {
      $rows[] = array(' ', ' ');
    }
    foreach ($parameters as $label => $key) {
      if (!empty($rowData->$key)) {
        $rows[] = array($label, $rowData->$key);
      }
    }
    $first = FALSE;
  }

  $build['log'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#empty' => t('No Unzer data available for this order.'),
  );

  return $build;
}
