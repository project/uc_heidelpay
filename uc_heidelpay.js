/**
 * @file
 * JS behaviours for the uc_heidelpay module.
 */
(function ($) {

  "use strict";

  Drupal.behaviors.uc_heidelpay = {
    attach: function (context, settings) {
    	uc_heidelpay_toggle_types();
	    $('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-ideal').click(function () {
	    	uc_heidelpay_toggle_types();
	    });
	    $('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-sofort').click(function () {
	    	uc_heidelpay_toggle_types();
	    });
    }
  };

})(jQuery);

function uc_heidelpay_get_cc_types() {
	return ['amex', 'visa', 'mastercard', 'jcb', 'diners'];
}

function uc_heidelpay_toggle_types() {
	// disable credit cards when sofort/ideal is checked
	var creditCardTypes = uc_heidelpay_get_cc_types();
	var creditCardsDisabled = jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-ideal').attr('checked')
		|| jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-sofort').attr('checked');
	if (creditCardsDisabled) {
    	for (var i = 0; i < creditCardTypes.length; i++) {
    		var type = creditCardTypes[i];
    		jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-' + type).attr('disabled', 'disabled');
        	jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-' + type).removeAttr('checked');
    	}
	}
	else {
    	for (var i = 0; i < creditCardTypes.length; i++) {
    		var type = creditCardTypes[i];
    		jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-' + type).removeAttr('disabled');
    	}
	}
	// disable sofort if ideal is checked
    if (jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-ideal').attr('checked')) {
    	jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-sofort').attr('disabled', 'disabled');
    	jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-sofort').removeAttr('checked');
    }
    else {
    	jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-sofort').removeAttr('disabled');
    }
	// disable ideal if sofort is checked
    if (jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-sofort').attr('checked')) {
    	jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-ideal').attr('disabled', 'disabled');
    	jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-ideal').removeAttr('checked');
    }
    else {
    	jQuery('#edit-uc-heidelpay-heidelpay-ideal-payment-methods-ideal').removeAttr('disabled');
    }
}


