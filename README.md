UC Heidelpay
============

INTRODUCTION
------------

This module integrates Unzer's (formerly Heidelpay) payment services (HSO) with Ubercart.
It implements only the HSO integration where credit card data is entered
on the Unzer website.
Entering credit card data directly on your shop is not supported.

INSTALLATION
------------

Follow the usual module installation procedure:

http://drupal.org/documentation/install/modules-themes/modules-7

SETTINGS
--------

The settings for the UC Heidelpay module are located at:

Administer -> Store administration -> Configuration -> Payment settings

Click on the "Payment methods" section and then expand the "Heidelpay
Settings" fieldset to view the settings.

__GENERAL SETTINGS__

_Sender ID_

This is your Unzer sender ID. You received it with your merchant account
data.

_User ID and Password_

This is your Unzer user ID and password. You received it with your
merchant account data.

_Channel ID_

The channel ID to use when sending payments. You received it with your
merchant account data.

_Order review submit button text_

Text on submit button.

_Payment message_

Here you can enter a message that will be displayed on top of the payment
page.

_CSS URL_

CSS URL that should be integrated to Unzer's payment page.

_Debug mode_

When enabled then debug messages will be logged to syslog.

__PAYMENT METHODS__

You can select the credit card logos that appear on the payment method
selection in checkout. Please note that this will not limit the credit
card types accepted by Unzer.

If you select Sofort or iDEAL then no other payment types can be selected.
Use a separate payment method configuration in this case.

PAYMENT PARAMETERS
------------------

_Enable test mode_

This will send transactions to test URL. All transactions are sent in
LIVE mode.

_Cancel order in Ubercart if cancelled during payment_

This will empty the shopper's cart if payment is cancelled.

_Payment page language_

Please enter the language code such as "en" or "de".

PAYMENT URLS
------------

_Callback URL_

The Unzer system will redirect the shopper to this URL. It should
end with "/cart/heidelpay/complete".

_Test URL_

URL for Unzer test system.

_Live URL_

URL for Unzer Live system.

_Signature password_

You can enter any random string. This will be used to sign transactions
and make sure payment responses from Unzer are valid.


__Using multiple Unzer configurations__

This module supports the usage of multiple Unzer merchant accounts.
By default only one account profile is created (named "Default"). To create
more accounts select "Heidelpay configuration" in Ubercart's store
configuration.



TESTING
-------

You can log in to the Unzer Test Merchant interface at
https://test-heidelpay.hpcgw.net/hip/
Login data is provided by Unzer on request.

Test access: https://dev.heidelpay.de/testumgebung

Example payment module configuration:

    Sender ID:     31HA07BC8142C5A171745D00AD63D182
    User ID:       31ha07bc8142c5a171744e5aef11ffd3
    User password: 93167DE7
    Channel ID:    31HA07BC8142C5A171744F3D6D155865

You can use the following credit card numbers to test the payment service:

    Visa:       4111111111111111  123
    MasterCard: 5105105105105100  123
    Amex:       375000000000007   1234
    Diners:     375000000000001   123
    JCB:        3530111333300000  123


PAYMENT IMAGES
--------------

The payment logos in images folder were created by Matthias Slovig and
are licensed under the "Attribution 4.0 International (CC BY 4.0)" license:
https://creativecommons.org/licenses/by/4.0/
